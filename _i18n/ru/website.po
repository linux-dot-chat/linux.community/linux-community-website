msgid ""
msgstr ""
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: en\n"
"X-Generator: Jekyll::PotLocalizationPlugin\n"

msgid "GNU/Linux® fora"
msgstr "Форумы (сообщества) GNU/Linux®"

msgid "May I view this page in another language?"
msgstr "Можно переключить язык?"

msgid "Russian"
msgstr "русский"

msgid "English"
msgstr "английский"

msgid "Yes"
msgstr "Да"


msgid "What resources are listed here?"
msgstr "Что я здесь вижу?"

msgid "An international directory of chat, wiki, forum GNU/Linux®-related resources."
msgstr "Сборник ссылок на чат, вики, форумы по линуксу на разных языках."

msgid "How do I submit a new or updated entry to this page?"
msgstr "Как добавить или обновить запись?"

msgid "Click here."
msgstr "Перейдите по ссылке."

msgid "Name"
msgstr "Название"

msgid "WWW"
msgstr "WWW"

msgid "IRC"
msgstr "IRC"

msgid "Distribution"
msgstr "Дистрибутив"

msgid "Description"
msgstr "Описание"

msgid "Type"
msgstr "Тип"

msgid "Language(s)"
msgstr "Язык(и)"

msgid "Webchat"
msgstr "Вебчат"

msgid "How do I search?"
msgstr "Как здесь искать?"

msgid "Type multiple keywords, such as distribution name, language, or word 'forum' or 'chat'. Do not type your GNU/Linux® question - this tool lists URLs only and does not search the forums or chats themselves."
msgstr "Вводите несколько ключевых слов, например, название дистрибутива, язык, слово «форум» или «вики» или «чат». Не пишите здесь текст своего вопроса - это только директория, а не поисковый моторчик."